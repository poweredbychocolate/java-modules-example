module services {
    requires model;
    requires daointerface;
    requires daoinmemory;
    uses dao.iterface.Dao;
    exports service;
}