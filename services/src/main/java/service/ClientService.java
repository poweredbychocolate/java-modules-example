package service;

import client.User;
import dao.in.memory.UserDao;
import dao.iterface.Dao;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClientService {
    private final Dao<User> userDao=new UserDao();


    public List<User> findAll() {
        return userDao.findAll();
    }

    public List<User> findByIds(List<Long> ids) {
        return userDao.findByIds(ids);
    }

    public Optional<User> findById(Long id) {
        return userDao.findById(id);
    }

    public User save(User toSave) {
        toSave.setId(userDao.generateKey());
        return userDao.save(toSave);
    }

    public List<User> saveAll(List<User> toSaveList) {

        return userDao.saveAll(toSaveList
                .stream()
                .peek(user -> user.setId(userDao.generateKey()))
                .collect(Collectors.toList()));
    }

    public Boolean update(User toUpdate) {
     return userDao.update(toUpdate);
    }

    public Boolean remove(User toRemove) {
        return userDao.remove(toRemove);
    }

}
