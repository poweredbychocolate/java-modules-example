package dao.iterface;

import java.util.List;

public interface Dao<T> extends SaveDao<T>,ReadDao<T> {

   Long generateKey();
   Boolean isValid(T toValid);
}
