package dao.iterface;

import java.util.List;

public interface SaveDao<T> {
    T save(T toSave);
    List<T> saveAll(List<T> toSaveList);
    Boolean update(T toUpdate);
    Boolean remove(T toRemove);
}
