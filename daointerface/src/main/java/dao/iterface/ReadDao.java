package dao.iterface;

import java.util.List;
import java.util.Optional;

public interface ReadDao<T> {
    List<T> findAll();
    List<T> findByIds(List<Long> ids);
    Optional<T> findById(Long id);
}
