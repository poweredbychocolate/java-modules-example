package client;

import lombok.*;
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@EqualsAndHashCode
public class User {
    @NonNull
    private Long id;
    @NonNull
    private String login;
}
