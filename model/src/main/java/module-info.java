module model {
    requires lombok;
    requires org.mapstruct.processor;
    exports client;
    exports shopping;
}