package shopping;

import client.Client;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class Bill {
    @NonNull
    private Long id;
    @NonNull
    private Client client;
    @NonNull
    private LocalDateTime time;
    @NonNull
    private List<Product> productList;
}
