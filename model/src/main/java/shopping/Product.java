package shopping;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
@ToString
public class Product {
    @NonNull
    private Long id;
    @NonNull
    private String name;
    @NonNull
    private Double price;
    private String description;
}
