module daoinmemory {
    requires model;
    requires daointerface;
    provides dao.iterface.Dao with dao.in.memory.UserDao;
    exports dao.in.memory;
}