package dao.in.memory;

import client.User;
import dao.iterface.Dao;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class UserDao implements Dao<User> {
    private final AtomicLong atomicKey = new AtomicLong(0);
    private final Map<Long, User> userMap = new ConcurrentHashMap<>();

    @Override
    public Long generateKey() {
        return atomicKey.incrementAndGet();
    }

    @Override
    public Boolean isValid(User toValid) {
        return true;
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(userMap.values());
    }

    @Override
    public List<User> findByIds(List<Long> ids) {
        return ids
                .stream()
                .map(id ->  userMap.getOrDefault(id,null ))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.ofNullable(userMap.getOrDefault(id,null));
    }

    @Override
    public User save(User toSave) {
        return userMap.put(toSave.getId(),toSave);
    }

    @Override
    public List<User> saveAll(List<User> toSaveList) {
        return toSaveList
                .stream()
                .peek(user ->  userMap.putIfAbsent(user.getId(),user))
                .collect(Collectors.toList());
    }

    @Override
    public Boolean update(User toUpdate) {
        try {
            userMap.merge(toUpdate.getId(), toUpdate, (user, user2) -> user2);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    @Override
    public Boolean remove(User toRemove) {
        return userMap.remove(toRemove.getId()) != null;
    }
}
