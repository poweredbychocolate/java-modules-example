package main.ui;

import client.User;
import service.ClientService;

public class MainApp {
    public static void main(String[] args) {
        ClientService clientService = new ClientService();

        System.out.println("1.User Test");
        User userBeforeSave = new User();
        userBeforeSave.setLogin("Test_Login");
        System.out.println(userBeforeSave);
        User userAfterSave = clientService.save(userBeforeSave);
        System.out.println(userAfterSave);
        User user = new User();
        user.setLogin("Login_test_22");
        clientService.save(user);
        user = new User();
        user.setLogin("Test453i");
        clientService.save(user);

        System.out.println("2.UserList");
        clientService.findAll().forEach(System.out::println);
    }
}
