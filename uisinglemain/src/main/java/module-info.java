module uisinglemain {
    requires model;
    requires services;
    requires daointerface;
    requires daoinmemory;
    uses service.ClientService;
}